<?php 
     $db = new PDO('mysql:host=localhost;dbname=mglsi_news', 'mglsi_user', 'passer');

     $query = "SELECT * FROM Categorie";
     $result = $db->query($query);
     $categories = $result->fetchAll(PDO::FETCH_ASSOC);

     if (isset($_GET['categorie'])) {
        $categorie_id = $_GET['categorie'];
        $query = "SELECT * FROM Article WHERE categorie = :categorie_id ORDER BY dateCreation DESC";
        $stmt = $db->prepare($query);
        $stmt->bindValue(':categorie_id', $categorie_id, PDO::PARAM_INT);
        $stmt->execute();
    } else {
        $query = "SELECT * FROM Article ORDER BY dateCreation DESC";
        $stmt = $db->query($query);
    }

    $articles = $stmt->fetchAll(PDO::FETCH_ASSOC);

    
?>
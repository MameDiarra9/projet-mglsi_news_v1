<?php 
    require "db.php";
?>
<!DOCTYPE html>
<html>
<head>
    <title>ESP News</title>
        <!-- Google Fonts -->
        <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&display=swap"
        rel="stylesheet"
        />
        <!-- bootstrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    </title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <div class="heading-container">
      <h4>ESP NEWS</h4>
    </div>

    <div class="menu-bar">
        <ul>
            <li><a href="index.php">Accueil</a></li>
            <?php
                foreach ($categories as $categorie) {
                    echo '<li><a href="index.php?categorie=' . $categorie['id'] . '">' . $categorie['libelle'] . '</a></li>';
                }
            ?>
        </ul>
    </div>

    <div class="content">
        <div class="row row-cols-1 row-cols-md-3 g-4">
            <?php foreach ($articles as $article): ?>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title"><?= $article['titre']; ?></h2>
                            <p class="card-text"><?= $article['contenu']; ?></p> 
                            <!-- <a href="index.php?article=< ?= $article['id']; ?>" class="btn btn-outline-danger">Voir plus</a> -->
                        </div>
                        <div class="card-footer">
                            <small class="text-body-secondary">Dernière modification : <?= $article['dateModification']; ?></small>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</body>
</html>
